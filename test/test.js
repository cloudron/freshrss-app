#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

const admin_username = 'admin', admin_password = 'changeme';

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    let browser, app;
    let athenticated_by_oidc = false;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    function baseUrl() {
        if (app.manifest.version === '1.4.0') return `https://${app.fqdn}/p`;
        return `https://${app.fqdn}`;
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('loginButton'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('passwordPlain')).sendKeys(password);
        await browser.findElement(By.id('loginButton')).click();
        await waitForElement(By.id('btn-subscription'));
    }

    async function loginOIDC(username, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/i/`);

        if (!athenticated_by_oidc) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();

            athenticated_by_oidc = true;
        }

        await waitForElement(By.id('btn-subscription'));
    }

    async function logout() {
        var logout_btn = By.xpath('//li/a[@class="signout"]');

        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('stream'));
        await browser.findElement(By.className('dropdown-toggle')).click();
        await waitForElement(logout_btn);
        await browser.findElement(logout_btn).click();
        await waitForElement(By.id('loginButton'));
    }

    async function addSubscription() {
        var url = 'https://blog.cloudron.io/rss/';
        const addUrl = app.manifest.version === '1.10.0' ? `${baseUrl()}/i/?c=subscription` : `${baseUrl()}/i/?c=subscription&a=add`;

        await browser.get(addUrl);
        await waitForElement(By.xpath('//input[@name="url_rss"]'));
        await browser.findElement(By.xpath('//input[@name="url_rss"]')).sendKeys(url);
        await browser.findElement(By.xpath('//form[@id="add_rss"]//button[text()="Add"]')).click();
        await waitForElement(By.xpath('//div[@id="notification" and contains(@class, "good")]'));
    }

    async function addUser(username, password) {
        await browser.get(`${baseUrl()}/i/?c=user&a=manage`);
        await waitForElement(By.id('new_user_name'));
        await browser.findElement(By.id('new_user_name')).sendKeys(username);
        await browser.findElement(By.id('new_user_passwordPlain')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Create"]')).click();
        await waitForElement(By.xpath('//div[@id="notification" and contains(@class, "good")]'));
    }

    async function enableApi() {
        await browser.get(`${baseUrl()}/i/?c=auth`);
        await waitForElement(By.id('api_enabled'));
        await browser.findElement(By.id('api_enabled')).click();
        await browser.findElement(By.xpath('//button[text()="Submit"]')).click();
        await browser.sleep(5000);
    }

    async function checkApiConfiguration() {
        await browser.get(`${baseUrl()}/api/`);
        await waitForElement(By.xpath('//dd[@id="greaderOutput" and contains(text(), "PASS")]'));
        await waitForElement(By.xpath('//dd[@id="feverOutput" and contains(text(), "PASS")]'));
    }

    async function subscriptionExists() {
        await browser.get(`${baseUrl()}/i/?get=c_1`);
        return waitForElement(By.xpath('//span[text()="Cloudron"]'));
    }

    async function getStaticExtensionFile() {
        const response = await superagent.get(`${baseUrl()}/ext.php?f=xExtension-StickyFeeds/static/script.js&t=js`)
            .buffer(true)
            .ok(() => true);
        expect(response.statusCode).to.be(200);
        expect(response.text).to.contain('sticky_feeds'); // relies on the buffer flag above
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    // No SSO
    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, admin_username, admin_password));
    it('can subscribe', addSubscription);
    it('can add users', addUser.bind(null, 'test', admin_password));
    it('can enable API', enableApi);
    it('can check configuration', checkApiConfiguration);
    it('subscription exists', subscriptionExists);
    it('can get static extension file', getStaticExtensionFile);
    it('can logout', logout);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // SSO
    it('install app (sso)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can make user Administrator', function () { execSync(`cloudron exec --app ${app.id} -- bash -c "php cli/reconfigure.php --default-user ${USERNAME}"`); });
    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can subscribe', addSubscription);
    it('can enable API', enableApi);
    it('can check configuration', checkApiConfiguration);
    it('subscription exists', subscriptionExists);
    it('can get static extension file', getStaticExtensionFile);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can check configuration', checkApiConfiguration);
    it('subscription exists', subscriptionExists);
    it('can get static extension file', getStaticExtensionFile);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can check configuration', checkApiConfiguration);
    it('subscription exists', subscriptionExists);
    it('can get static extension file', getStaticExtensionFile);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', function () { execSync('cloudron install --appstore-id org.freshrss.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can make user Administrator', function () { execSync(`cloudron exec --app ${app.id} -- bash -c "php cli/reconfigure.php --default-user ${USERNAME}"`); });
    it('can subscribe', addSubscription);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can add users', addUser.bind(null, 'test', admin_password));
    it('subscription exists', subscriptionExists);
    it('can get static extension file', getStaticExtensionFile);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
