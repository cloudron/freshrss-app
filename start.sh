#!/bin/bash

set -eu

mkdir -p /run/php/session /app/data/extensions

if ! [ -f /app/data/.installed ]; then
    echo "==> Fresh installation, setting up..."
    rsync -a /app/code/data-orig/ /app/data/
    php cli/do-install.php \
      --environment production --default_user admin \
      --db-type mysql --db-host "${CLOUDRON_MYSQL_HOST}" \
      --db-user "${CLOUDRON_MYSQL_USERNAME}" --db-password "${CLOUDRON_MYSQL_PASSWORD}" \
      --db-base "${CLOUDRON_MYSQL_DATABASE}" --db-prefix "" \
      --disable_update

    if [[ -z "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        php cli/create-user.php --user admin --password changeme --language en
    fi

    touch /app/data/.installed
    echo "==> Done."
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

echo "==> Symlinking log file"
rm -f /app/data/users/_/log_api.txt
touch /tmp/log_api.txt
ln -s /tmp/log_api.txt /app/data/users/_/log_api.txt

# We have to copy instead of symlinking extensions (see #2)
echo "==> Copying packaged extensions"
for f in $(ls /app/code/extensions-orig); do
    rm -rf "/app/data/extensions/$f"
    cp -r "/app/code/extensions-orig/$f" "/app/data/extensions"
done

echo "==> Updating config file"
if [[ -z "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    extra_args="--default_user admin"
    [[ ! -f /app/data/.oauth_crypto_passphrase ]] && openssl rand -base64 42 > /app/data/.oauth_crypto_passphrase
    export OIDC_CRYPTO_PASSPHRASE=$(</app/data/.oauth_crypto_passphrase) # used in apache config
else
    extra_args="--auth_type http_auth"
fi

php cli/reconfigure.php ${extra_args} --base_url "https://${CLOUDRON_APP_DOMAIN}" \
    --db-type mysql --db-host "${CLOUDRON_MYSQL_HOST}" \
    --db-user "${CLOUDRON_MYSQL_USERNAME}" --db-password "${CLOUDRON_MYSQL_PASSWORD}" \
    --db-base "${CLOUDRON_MYSQL_DATABASE}" --db-prefix "" \
    --disable_update

echo "==> Setting permissions"
chown -R www-data.www-data /run/php /app/data /tmp/log_api.txt

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -D FOREGROUND $([[ -n "$CLOUDRON_OIDC_ISSUER" ]] && echo '-D OIDC_ENABLED')
