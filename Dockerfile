FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN apt-get update && apt-get install --no-install-recommends -y libapache2-mod-auth-openidc && rm -rf /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=FreshRSS/FreshRSS versioning=semver
ARG FRESHRSS_VERSION=1.26.0

RUN curl -L https://github.com/FreshRSS/FreshRSS/archive/${FRESHRSS_VERSION}.tar.gz | tar -zxvf - --strip-components=1 && \
    mv data data-orig && ln -s /app/data data

# official extensions (https://github.com/FreshRSS/Extensions/commits/master)
RUN wget https://github.com/FreshRSS/Extensions/archive/a82e080a1e68eb34539959a852fc5e800b7346dd.tar.gz -O - | tar -xz --strip-components=1 -C /app/code/extensions && \
    mv /app/code/extensions /app/code/extensions-orig && \
    ln -s /app/data/extensions /app/code/extensions

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/freshrss.conf /etc/apache2/sites-enabled/freshrss.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN a2enmod headers expires deflate mime dir rewrite setenvif auth_openidc

RUN rm -rf /var/lib/php && ln -s /run/php /var/lib/php

RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/php/session && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
