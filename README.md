# FreshRSS Cloudron App

This repository contains the Cloudron app package source for [FreshRSS](http://www.freshrss.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.freshrss.cloudronapp)

or using the [Cloudron command line tooling](https://git.cloudron.io/cloudron/cloudron-cli/)

```
cloudron install --appstore-id org.freshrss.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://git.cloudron.io/cloudron/cloudron-cli/).

```
cd freshrss-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the repos are still ok. The tests expect port 29418 to be available.

```
cd freshrss-app/test

npm install
PATH=$PATH:node_modules/.bin mocha --bail test.js
```

## Debugging


* Update your data/config.php with the following, to get more debugging information:

```
'environment' => 'development',

```

* Check your logs:
  * Web server / PHP logs
  * FreshRSS API log in ./data/users/_/log_api.txt
  * FreshRSS logs in ./data/users/you/log.txt

