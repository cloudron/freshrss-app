[0.1.0]
* Initial version

[0.2.0]
* Updated to base image 0.10.0
* Supporting extensions

[0.3.0]
* No longer overwriting all config changes
* Using cli scripts
* Using scheduler for periodic task
* Removed update menu item

[0.4.0]
* Updated to FreshRSS 1.6.3
* Removed most custom patches (FreshRSS includes those now)

[0.4.1]
* Add parameter to allow for encoded slashes (thanks @Richard)
* Updated description (thanks @Girish)

[0.5.0]
* Not using mysql port as this causes an error
* Storing sessions in /run
* Storing api logs in /tmp
* Set some php options

[0.6.0]
* Updated to FreshRSS 1.7.0

[0.7.0]
* Updated to FreshRSS 1.8.0

[0.8.0]
* Update to FreshRSS 1.9.0

[0.9.0]
* Update to FreshRSS 1.10.2

[0.10.0]
* Better apache configs
* Update extensions

[1.0.0]
* Fix issue where static extension assets were not served

[1.1.0]
* Update FreshRSS to 1.11.0

[1.1.1]
* Update FreshRSS to to 1.11.1

[1.1.2]
* Update FreshRSS to 1.11.2

[1.2.0]
* Use latest base image

[1.3.0]
* Update FreshRSS to 1.12.0
* Features
  * Ability to add labels (custom tags) to articles #928
  * Handle article tags containing spaces, as well as comma-separated tags #2023
  * Handle authors containing spaces, as well as comma or semi-colon separated authors #2025
  * Searches by tag, author, etc. accept Unicode characters #2025
  * New option to disable cache for feeds with invalid HTTP caching #2052
* UI
  * New theme Swage #2069
  * Click on authors to initiate a search by author #2025
  * Fix CSS for button alignments in older Chrome versions #2020
* Security
  * Improved flow for password change (avoid error 403) #2056
  * Allow dot . in username (best to avoid, though) #2061
* Performance
  * Remove some counterproductive preload / prefetch rules #2040
  * Improved fast flush (earlier transfer, fetching of resources, and rendering) #2045

[1.4.0]
* Update FreshRSS to 1.13.0
* [Full changelog](https://github.com/FreshRSS/FreshRSS/blob/1.13.0/CHANGELOG.md)
* Improvements to the Google Reader API #2093
* Support for Vienna RSS (client for Mac OS X) #2091
* Ability to import XML files exported from Tiny-Tiny-RSS #2079
* Ability to show all the feeds that have a warning #2146
* Share with Pinboard #1972

[1.5.0]
* Fix security issue where root directory was exposed
* Enable mod rewrite for mobile apps like FeedMe to work

[1.5.1]
* Update FreshRSS to 1.13.1

[1.6.0]
* Update FreshRSS to 1.14.0
* Update extensions to 0812ee05c24c

[1.6.1]
* Update FreshRSS to 1.14.1
* Fix load more articles when using ascending order #2314
* Fix the use of arrow keyboard keys for shortcuts #2316
* Fix control+click or middle-click for opening articles in a background tab #2310
* Fix the naming of the option to unfold categories #2307
* Fix shortcut problem when using unfolded articles #2328
* Fix auto-hiding articles #2323
* Fix scroll functions with Edge #2337
* Fix drop-down menu warning #2353
* Fix delay for individual mark-as-read actions #2332
* Fix scroll functions in Edge #2337

[1.6.2]
* Update FreshRSS to 1.14.2
* Fix minor code syntax warning in API #2362

[1.6.3]
* Update FreshRSS to 1.14.3
* New configuration page for each category #2369
* Update shortcut configuration page #2405
* CSS style for printing #2149
* Do not hide multiple <br /> tags #2437
* Updated to jQuery 3.4.1 (only for statistics page) #2424

[1.7.0]
* Update FreshRSS to 1.15.0
* New archiving method, including maximum number of articles per feed, and settings at feed, category, global levels #2335
* New option to control category sort order #2592
* New option to display article authors underneath the article title #2487
* Add e-mail capability #2476, #2481
* Ability to define default user settings in data/config-user.custom.php #2490
* Including default feeds #2515
* Allow recreating users if they still exist in database #2555
* Add optional database connection URI parameters #2549, #2559
* Allow longer articles with MySQL / MariaDB (up to 16MB compressed instead of 64kB) #2448
* Add support for terms of service #2520
* Add sharing with Lemmy #2510

[1.7.1]
* Update FreshRSS to 1.15.1

[1.7.2]
* Update FreshRSS to 1.15.2

[1.7.3]
* Update FreshRSS to 1.15.3

[1.8.0]
* Update FreshRSS to 1.16.0
* [Full changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.16.0)
* Allow multiple users to have administration rights #2096
* Preview the CSS rule to retrieve full article content #2778
* Improve CSS selector ordering in the full-text retrieval (lib_phpQuery) #2874
* New search option !date: allowing to exclude any date interval #2869
* New option to show all articles in the favourites view #2434
* Allow feed to be actualized just after being truncated #2862
* Fallback to showing a GUID when an article title is empty #2813

[1.9.0]
* Use latest base image 2.0.0

[1.9.1]
* Update FreshRSS to 1.16.2
* [Full changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.16.1)
* Add the possibility to filter by feed IDs #2892
  * like f:123 more-search or multiple feed IDs like f:123,234,345 more-search or an exclusion like !f:456,789 more-search
* Show users last activity date #2936
* Ability to follow HTML redirections when retrieving full article content #2985

[1.10.0]
* Add forum url and update screenshot links

[1.11.0]
* Update FreshRSS to 1.17.0
* [Full changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.17.0)
* New tag management page #3121
* New page to add feeds and categories #3027
* Add a way to disable/enable users #3056
* Fix special characters in user queries #3037
* Hide feed credentials when adding a new feed #3099
* Trim whitespace for feed passwords #3158
* Updated PHPMailer library to 6.1.6 #3024
* Add blogger.com to the default list of forced HTTPS #3088

[1.12.0]
* Update base image to v3
* Update PHP to 7.4

[1.13.0]
* Update FreshRSS to 1.18.0
* [Full changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.18.0)
* Allow parallel requests #3096 - Much faster manual feeds refresh
* Reload full article content when an article has changed #3506
* New share article link to clipboard #3330
* Improved OPML import of feeds with multiple categories #3286
* Add a content action parameter to work with CSS selector #3453
* New cURL options per feed: proxy, cookie, user-agent #3367, #3494, #3516
* Do not import feeds causing database errors (e.g. due to conflicting HTTP redirections) ##3347

[1.13.1]
* Fix apache config to log the client IP

[1.13.2]
* Update FreshRSS to 1.18.1
* Support standard HTTP 410 Gone by disabling (muting) gone feeds #3561
* Supported by Newsboat 2.24+ #3574
* Supported by RSS Guard #3627
* Allow Unicode for shortcuts #3548

[1.14.0]
* Update FreshRSS to 1.19.0
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.19.0)

[1.14.1]
* Update FreshRSS to 1.19.1
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.19.1)

[1.14.2]
* Update apache configs

[1.14.3]
* Update FreshRSS to 1.19.2
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.19.2)
* Improve dropdown menus on mobile view #4141, #4128
* Fix regression regarding keeping read state after seeing favourites / labels #4178
* Lots of code improvements, including improved support of PHP 8.1

[1.15.0]
* Update FreshRSS to 1.20.0
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.20.0)
* New Web scraping feature HTML+XPath for Web pages without any RSS/ATOM feed #4220
* Add support for Dynamic OPML #4407
* New search engine supporting (nested) parentheses, also with negation #4378
* Allow many (50k+) feeds #4347 and other performance improvements
* New option to exclude some DOM elements with a CSS Selector when retrieving an article full content #4501
* New option to automatically mark as read gone articles #4426
* 2 new themes and plenty of UI improvements
* Supported by Fluent Reader Lite client on Android and iOS #4595
* Several bug fixes

[1.15.1]
* Update FreshRSS to 1.20.1
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.20.1)
* Add support for custom XPath date/time format #4703
* Add default redirect when authenticating #4778
* Force default user before rendering login page #4620

[1.15.2]
* Update FreshRSS to 1.20.2
* Update Cloudron base image to 4.0.0
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.20.2)
* Fix security vulnerability in ext.php #4928 reported by @c3l3si4n

[1.15.3]
* Update FreshRSS extensions repo to f66efcf5f

[1.16.0]
* Update FreshRSS to 1.21.1
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.21.0)
* New XML+XPath mode for fetching XML documents when there is no RSS/ATOM feed #5076
* Better support of feed enclosures (image / audio / video attachments) #4944
* User-defined time-zone #4906
* New CLI script cli/sensitive-log.sh to help e.g. Apache clear logs for sensitive information such as credentials #5001
* Mark some themes as tentatively deprecated: BlueLagoon, Flat, Screwdriver #4807
* Many UI improvements

[1.17.0]
* Update base image to 4.2.0

[1.18.0]
* Update FreshRSS to 1.22.0
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.22.0)
* Rework trusted proxies
* Improve scaling with many feeds and long processes, reduce database locks
* Fix many bugs and regressions
* Improve themes Origine (also with automatic dark mode), Nord, etc.
* Several UI / UX improvements
* New languages Hungarian, Latvian, Persian

[1.19.0]
* Implement OIDC login

[1.19.1]
* Update FreshRSS to 1.22.1
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.22.1)
* Fix regression in extensions translations (i18n)
* Better identification of proxied client IP

[1.20.0]
* Update FreshRSS to 1.23.0
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.23.0)
* New Important feeds group in the main view, with corresponding new priority level for feeds #5782
* Entries from important feeds are not marked as read during scroll, during focus, nor during Mark all as read
* Add filter actions (auto mark as read) at category level and at global levels #5942
* Increase SQL fields length to maximum possible #5788, #5570
* Many bug fixes
* Soft require Apache 2.4+ (but repair minimal compatibility with Apache 2.2)
* Upgraded extensions require FreshRSS 1.23.0+ Extensions#181

[1.20.1]
* Update FreshRSS to 1.23.1
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.23.0)
* Fix crash regression with the option Max number of tags shown #5978
* Fix crash regression when enabling extensions defined by old FreshRSS installations #5979
* Fix crash regression during export when using MySQL #5988
* More robust assignment of categories to feeds #5986

[1.21.0]
* Update FreshRSS to 1.24.0
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.24.0)
* New shareable user query mechanism to share lists of articles by HTML, RSS, OPML
* New CLI for database backup & restore
* New JSON scraping mode to consume JSON data and JSON Feeds
* New support for HTTP POST
* New option to automatically add labels to incoming articles
* New button to download a feed configuration as OPML

[1.21.1]
* Update FreshRSS to 1.24.1
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.24.1)
* New button to export OMPL of a category
* Many bug fixes

[1.21.2]
* Update FreshRSS to 1.24.2
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.24.2)
* New global option to automatically add articles to favourites
* New option to share articles from the article title line
* Add core extensions, shipped by default: UserCSS and UserJS
* Security: Force log out of users when they are disabled
* Last version supporting PHP 7.4 and initial support for PHP 8.4+
* Many bug and regression fixes

[1.21.3]
* Update FreshRSS to 1.24.3
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.24.3)
* Fix mark-as-read from user query #6738
* Fix regression for shortcut to move between categories #6741
* Fix feed title option #6771
* Fix XPath for HTML documents with broken root (used by CSS selectors to fetch full content) #6774
* Fix UI regression in Mapco/Ansum themes #6740
* Fix minor style bug with some themes #6746
* Fix export of OPML information for date format of JSON and HTML+XPath feeds #6779
* OpenID Connect better definition of session parameters #6730

[1.22.0]
* checklist added to CloudronManifest

[1.23.0]
* Update FreshRSS to 1.25.0
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.25.0)
* Features
* Bug fixing
* API
* Compatibility
* Deployment
* SimplePie
* Security
* UI
* Extensions
* I18n
* Misc.

[1.24.0]
* Update FreshRSS to 1.26.0
* [Full Changelog](https://github.com/FreshRSS/FreshRSS/releases/tag/1.26.0)
* Add order-by options to sort articles by received date (existing, default), publication date, title, link, random
* Allow searching in all feeds, also feeds only visible at category level with &get=A, and also those archived with &get=Z
* UI accessible from user-query view
* New shortcuts for adding user labels to articles
* Several improvements and bug fixes

